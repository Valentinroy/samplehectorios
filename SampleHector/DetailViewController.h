//
//  DetailViewController.h
//  SampleHector
//
//  Created by Valentin ROY on 26/05/2016.
//  Copyright © 2016 Hector. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Hector.h"

@interface DetailViewController : UIViewController

- (instancetype)initWithHector:(Hector*)hector;

@end
