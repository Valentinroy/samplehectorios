//
//  HectorCentral.h
//  HectorLibrary
//
//  Created by Valentin ROY on 25/05/2016.
//  Copyright © 2016 Hector. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Hector.h"

/*!
 * HectorCentralState
 * An integer that represent state of the current central
 */
typedef NS_ENUM(NSInteger, HectorCentralState) {
    /** On */
    HectorCentralStateBLEOn = 0,
    /** Off */
    HectorCentralStateBLEOff,
    /** Not supported */
    HectorCentralStateUnsupported,
    /** Not authorized / Not enabled */
    HectorCentralStateUnauthorized,
    /** Unknow */
    HectorCentralStateUnknown
};

/*!
 * HectorCentralDelegate
 * Protocol
 */
@protocol HectorCentralDelegate <NSObject>

@optional
/*!
 * Hector central did discover an Hector
 * @param hector Hector which has been discovered
 */
- (void)hectorCentralDidDiscoverHector:(Hector*)hector;

/*!
 * Hector central just connect to an Hector
 * @param hector Hector which has been connected
 */
- (void)hectorCentralDidConnectHector:(Hector*)hector;

/*!
 * Hector central just discconnect an Hector
 * @param hector Hector which as been disconnect
 * @param error Error occured when disconnect
 */
- (void)hectorCentralDidDisconnectHector:(Hector*)hector error:(NSError *)error;

/*!
 * Hector central can't connect to Hector
 * @param hector Hector which is not reachable
 * @param error Error occured when trying to connect
 */
- (void)hectorCentralDidDidFailToConnectHector:(Hector*)hector error:(NSError *)error;

/*!
 * Hector central state did update
 * @param state The state of bluetooth
 */
- (void)hectorCentralDidUpdateState:(HectorCentralState)state;
@end

/*!
 * Hector
 * Class that help you to scan, connect and disconnect to an Hector
 */
@interface HectorCentral : NSObject

@property (nonatomic, assign) HectorCentralState state;
@property (nonatomic, weak) id<HectorCentralDelegate> delegate;

+ (instancetype)sharedInstance;

/*!
 * Start to scan Hector arround your position.
 *
 * @note Scan won't stop automatically, you must call -(void)stopScan
 */
- (void)startScan;

/*!
 * Stop scanning of the central
 */
- (void)stopScan;

/*!
 * Create a Bluetooth connection with Hector in parameter
 * @param hector The device to connect with
 */
- (void)connectHector:(Hector*)hector;

/*!
 * Disconnect the hector from central
 * @param hector The device to discconnect
 */
- (void)disconnectHector:(Hector*)hector;



@end
