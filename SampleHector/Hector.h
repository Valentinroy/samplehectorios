//
//  Hector.h
//  HectorLibrary
//
//  Created by Valentin ROY on 25/05/2016.
//  Copyright © 2016 Hector. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

/*!
 * HectorSensorType
 * An integer that represent type of sensor
 */
typedef NS_ENUM(NSInteger, HectorSensorType) {
    
    /** Temperature */
    HectorSensorTypeTemperature = 0,
    /** Humidity */
    HectorSensorTypeHumidity,
    /** Pressure */
    HectorSensorTypePressure
};

/*!
 * HectorState
 * An integer that represent state of Hector
 */
typedef NS_ENUM(NSInteger, HectorState) {
    
    /** Disconnected */
    HectorStateDisconnected = 0,
    /** Connected */
    HectorStateConnected
};

/*!
 * HectorDelegate
 * Protocol
 */
@protocol HectorDelegate <NSObject>

@required
- (void)hectorDidInitialize:(NSError*)error;

@optional
- (void)hectorDidReadTemperature:(float)temperature;
- (void)hectorDidReadHumidity:(float)humidity;
- (void)hectorDidReadPressure:(float)pressure;
- (void)hectorDidReadMinTemperature:(float)minTemperature;
- (void)hectorDidReadMaxTemperature:(float)maxTemperature;
- (void)hectorDidReadMinHumidity:(float)minHumidity;
- (void)hectorDidReadMaxHumidity:(float)maxHumidity;
- (void)hectorDidReadBattery:(float)battery;
- (void)hectorDidReadName:(NSString*)name;
- (void)hectorDidReadNumberEntries:(NSInteger)nbValues forType:(HectorSensorType)type;
- (void)hectorHistoryDidLoad:(NSArray*)values forType:(HectorSensorType)type error:(NSError*)error;

/*!
 * This method is call when the downloading progress (history) has changed
 * @param numberOfValueDownloaded Number of value of temperature downloaded
 */
- (void)hectorHistoryNumberDownloaded:(int)numberOfValueDownloaded;


/*!
 * This method is call when humidity external accessory is connected and his value did read
 * @param humidity the relative value of humidity taken by external accessory
 * @param error error occured while reading value
 */
- (void)hectorDidReadHumidityPic:(float)humidity error:(NSError*)error;

@end

/*!
 * Hector
 * Object that represent an Hector and help you to configure and communicate with physical product
 */
@interface Hector : NSObject

@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSNumber *rssi;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) BOOL alertHour;
@property (nonatomic, assign) BOOL alertPressure;
@property (nonatomic, assign) BOOL alertHumidity;
@property (nonatomic, assign) BOOL alertTemperature;
@property (nonatomic, assign) BOOL isConnected;
@property (nonatomic, assign) HectorState state;
@property (nonatomic, weak) id<HectorDelegate> delegate;
@property (nonatomic, assign) NSInteger period;
@property (nonatomic, assign) float batteryLevel;
@property (nonatomic, assign) float temperature;
@property (nonatomic, assign) float humidity;
@property (nonatomic, assign) float pressure;
@property (nonatomic, assign) int minTemperature;
@property (nonatomic, assign) int maxTemperature;
@property (nonatomic, assign) int minHumidity;
@property (nonatomic, assign) int maxHumidity;
@property (nonatomic, strong) NSString *hardwareVersion;
@property (nonatomic, strong) NSString *softwareVersion;
@property (nonatomic, assign) BOOL humidityPicIsAvailable;

- (instancetype)initWithIdentifier:(NSString*)identifier name:(NSString*)name alert:(int)alert rssi:(NSNumber*)rssi;
- (void)setBluetoothPeripheral:(CBPeripheral*)peripheral;

/*!
 * Active or disable notification on a value change, for a type
 * @param active Boolean that indicate if notification is active or not
 * @param type HectorSensorType is the sensor to active in notification
 */
- (void)setNotification:(BOOL)active forType:(HectorSensorType)type;

/*!
 * Get measurement period
 */
- (void)getMeasurementPeriod;

/*!
 * Set measurement period
 * @note IT WILL ERASE ALL ENTRIES (for all sensor) IN HISTORICAL
 */
- (void)setDefaultMeasurementPeriod;

/*!
 * Set time
 * @note IT WILL ERASE ALL ENTRIES (for all sensor) IN HISTORICAL
 */
- (void)setOnTime;

/*!
 * Change period of automatique measure taken by Hector
 * @param periodParam One of next case :
 * case 1: 60min
 * case 2: 30min
 * case 3: 20min
 * case 4: 15min
 * case 5: 10min
 * case 6: 5min
 * case 7: 1min
 * @note IT WILL ERASE ALL ENTRIES (for all sensor) IN HISTORICAL
 */
- (void)setMeasurementPeriod:(int)periodParam;

/*!
 * Get history for a type of sensor
 * @param fromIndex Number representing start index in list of saved value
 * @param toIndex Number representing end index in list of saved value (MAX 500 values saved)
 * @param type Type of sensor to extract historic
 */
- (void)getHistoryFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex forType:(HectorSensorType)type;

/*!
 * Get number of values saved in Hector for a sensor type
 * Each sensor have is space to save value
 * @param type Type of sensor
 */
- (void)readNumberEntriesForType:(HectorSensorType)type;

/*!
 * Initialize services of Hector
 */
- (void)initializeServices;

/*!
 * Rename the sensor
 * @param name New name to write into the sensor
 */
- (void)rename:(NSString*)name;

/*!
 * Write min threshold in Hector for specific sensor
 * @param min Value to setup as minimum
 * @param type Type of sensor to setup
 */
- (void)writeMin:(float)min forType:(HectorSensorType)type;

/*!
 * Write max threshold in Hector for specific sensor
 * @param max Value to setup as maximum
 * @param type Type of sensor to setup
 */
- (void)writeMax:(float)max forType:(HectorSensorType)type;

/*!
 * Read information saved in Hector : min / max threshold, name, instant temperature and humidity
 */
- (void)readInformations;

/*!
 * Read value of the external humidity accessory
 */
- (void)getHumidityPic;

/*!
 * Read value of the battery in volts
 */
- (void)getBattery;

/*!
 * Read instant temperature
 */
- (void)readTemperature;

/*!
 * Read instant humidity
 */
- (void)readHumidity;

/*!
 * Read instant pressure
 */
- (void)readPressure;

@end
