//
//  HistoryValue.h
//  Hector
//
//  Created by Valentin ROY on 24/04/2015.
//  Copyright (c) 2015 Popup. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    
    SensorTypeTemperature = 0,
    SensorTypeHumidity,
    SensorTypePressure
    
} SensorType;

@interface HistoryValue : NSObject

@property (nonatomic, assign) NSInteger index;
@property (nonatomic, assign) float temperature;
@property (nonatomic, assign) float humidity;
@property (nonatomic, assign) float pressure;
@property (nonatomic, strong) NSString *graphFormattedDateString;
@property (nonatomic, strong) NSString *dateString;
@property (nonatomic, strong) NSString *dayOfMonthString;
@property (nonatomic, strong) NSString *dayOfWeekString;
@property (nonatomic, strong) NSString *monthAndYearString;
@property (nonatomic, strong) NSString *hourString;
@property (nonatomic, assign) BOOL isOutOfRulesTemp;
@property (nonatomic, assign) BOOL isOutOfRulesHum;
@property (nonatomic, assign) BOOL isToday;
@property (nonatomic, assign) BOOL isYesterday;

- (id)initWithTemperature:(double)data dateFormatter:(NSDateFormatter*)dateFormatter index:(NSInteger)indexParam;
- (id)initWithHumidity:(uint16_t)data dateFormatter:(NSDateFormatter*)dateFormatter index:(NSInteger)indexParam;
- (id)initWithPressure:(uint32_t)data dateFormatter:(NSDateFormatter*)dateFormatter index:(NSInteger)indexParam;

- (void)populateWithHumidityValue:(HistoryValue*)humValue;
- (float)getTemperature;
- (float)getHumidity;
- (float)getPressure;
- (NSString*)getFormattedDate;
- (NSString*)getDateString;
- (NSString*)getDay;
- (NSString*)getMonthYear;
- (NSString*)getDayLetter;
- (NSString*)getHour;

@end
