//
//  ViewController.m
//  SampleHector
//
//  Created by Valentin ROY on 26/05/2016.
//  Copyright © 2016 Hector. All rights reserved.
//

#import "ViewController.h"
#import "HectorCentral.h"
#import "DetailViewController.h"

@interface ViewController () <UITableViewDelegate, UITableViewDataSource, HectorCentralDelegate>
@property (nonatomic, strong) UIButton *scanButton;
@property (nonatomic, strong) NSMutableArray *hectors;
@property (nonatomic, assign) BOOL scanning;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) Hector *hector;
@end

@implementation ViewController

- (instancetype)init {
    
    if (self = [super init]) {
        
        _scanning = NO;
        _hectors = [NSMutableArray array];
        
        [[HectorCentral sharedInstance] setDelegate:self];
    }
    return self;
}

- (void)loadView {
    [super loadView];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    _scanButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, 50.0)];
    [_scanButton setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [_scanButton addTarget:self action:@selector(scanAction) forControlEvents:UIControlEventTouchUpInside];
    [_scanButton setTitle:@"Scan" forState:UIControlStateNormal];
    [_scanButton setBackgroundColor:[UIColor grayColor]];
    [self.view addSubview:_scanButton];
    
    // TableView
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 50.0, self.view.frame.size.width, self.view.frame.size.height - 50.0)];
    [_tableView setAutoresizingMask:UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    [self.view addSubview:_tableView];
}

#pragma mark - View cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    
    if (_hector) {
        [[HectorCentral sharedInstance] disconnectHector:_hector];
        _hector = nil;
    }
}

#pragma mark - Actions
- (void)scanAction {
    
    if (_scanning) return;
    _scanning = YES;
    
    [_scanButton setTitle:@"Scanning..." forState:UIControlStateNormal];
    [_hectors removeAllObjects];
    [_tableView reloadData];
    
    [[HectorCentral sharedInstance] startScan];
    
    [self performSelector:@selector(stopScan) withObject:nil afterDelay:10];
}

- (void)stopScan {
    
    _scanning = NO;
    [[HectorCentral sharedInstance] stopScan];
    [_scanButton setTitle:@"Scan" forState:UIControlStateNormal];
}

#pragma mark - HectorCentralDelegate
- (void)hectorCentralDidDiscoverHector:(Hector *)hector {
    
    [_hectors addObject:hector];
    [_tableView reloadData];
}

- (void)hectorCentralDidConnectHector:(Hector *)hector {

    [_scanButton setTitle:@"Scan" forState:UIControlStateNormal];
    
    DetailViewController *viewController = [[DetailViewController alloc] initWithHector:hector];
    [[self navigationController] pushViewController:viewController animated:YES];
}

- (void)hectorCentralDidUpdateState:(HectorCentralState)state {
    
    switch (state) {
        case HectorCentralStateBLEOn:
            break;
        case HectorCentralStateBLEOff:{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Bluetooth"
                                                            message:@"Veuillez activer votre Bluetooth"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Fermer"
                                                  otherButtonTitles:nil];
            
            [alert show];
            break;
        }
        case HectorCentralStateUnsupported: {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur"
                                                            message:@"Non compatible Bluetooth 4.0 (Low Energy)"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Fermer"
                                                  otherButtonTitles:nil];
            
            [alert show];
            break;
        }
        case HectorCentralStateUnauthorized:
            break;
        case HectorCentralStateUnknown:
            break;
        default:
            break;
    }
}

- (void)hectorCentralDidDisconnectHector:(Hector *)hector error:(NSError *)error {
    
    [[self navigationController] popToViewController:self animated:YES];
}

- (void)hectorCentralDidDidFailToConnectHector:(Hector *)hector error:(NSError *)error {
    
}

#pragma mark - UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40.0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_hectors count];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    _hector = [_hectors objectAtIndex:indexPath.row];
    
    [[HectorCentral sharedInstance] stopScan];
    
    [_scanButton setTitle:@"Connecting..." forState:UIControlStateNormal];
    [[HectorCentral sharedInstance] connectHector:_hector];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 10.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        
        if (_hector && _hector.state != HectorStateConnected) {
            
            [[self navigationController] popToRootViewControllerAnimated:YES];
            [[HectorCentral sharedInstance] disconnectHector:_hector];
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Connexion", @"")
                                                                message:NSLocalizedString(@"Impossible de se connecter à se périphérique", @"")
                                                               delegate:nil
                                                      cancelButtonTitle:NSLocalizedString(@"Fermer", @"")
                                                      otherButtonTitles:nil];
            [alertView show];
        }
    });
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *identifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (nil == cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    Hector *hector = [_hectors objectAtIndex:indexPath.row];
    [[cell textLabel] setText:hector.name];
    
    return cell;
}

@end
