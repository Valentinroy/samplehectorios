//
//  DetailViewController.m
//  SampleHector
//
//  Created by Valentin ROY on 26/05/2016.
//  Copyright © 2016 Hector. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController () <HectorDelegate>
@property (nonatomic, strong) Hector *hector;
@property (nonatomic, assign) NSInteger nbEntriesTemperature;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *temperatureLabel;
@property (nonatomic, strong) UILabel *humidityLabel;
@property (nonatomic, strong) UILabel *humidityPicLabel;
@property (nonatomic, strong) UILabel *historyLabel;
@property (nonatomic, strong) UITextView *consolTextView;
@end

@implementation DetailViewController

- (instancetype)initWithHector:(Hector*)hector {
    
    if (self = [super init]) {
        
        _hector = hector;
        
        [_hector setDelegate:self];
        [_hector initializeServices];
    }
    return self;
}

- (void)loadView {
    [super loadView];

    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    // Name
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 0.0, self.view.frame.size.width - 40.0, 30.0)];
    [[self view] addSubview:_titleLabel];
    
    // Temperature
    _temperatureLabel = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 30.0, self.view.frame.size.width - 40.0, 30.0)];
    [[self view] addSubview:_temperatureLabel];
    
    // Humidity
    _humidityLabel = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 60.0, self.view.frame.size.width - 40.0, 30.0)];
    [[self view] addSubview:_humidityLabel];
    
    // Humidity Pic
    _humidityPicLabel = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 90.0, self.view.frame.size.width-40.0, 30.0)];
    [_humidityPicLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [_humidityPicLabel setText:@"--%"];
    [_humidityPicLabel setTextAlignment:NSTextAlignmentCenter];
    [_humidityPicLabel setFont:[UIFont systemFontOfSize:40]];
    [[self view] addSubview:_humidityPicLabel];
    
    UIButton *humidityPicButton = [[UIButton alloc] initWithFrame:CGRectMake(20.0, 130.0, self.view.frame.size.width-40.0, 60.0)];
    [humidityPicButton setBackgroundColor:[UIColor grayColor]];
    [humidityPicButton setTitle:@"Faire une mesure" forState:UIControlStateNormal];
    [humidityPicButton addTarget:self action:@selector(takeHumidityPicAction) forControlEvents:UIControlEventTouchUpInside];
    [humidityPicButton setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleLeftMargin];
    [humidityPicButton setHidden:YES];
    [[self view] addSubview:humidityPicButton];
    
    // History
    /*
    _historyLabel = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 200.0, self.view.frame.size.width - 40.0, 30.0)];
    [_historyLabel setText:@"Reading historic..."];
    [[self view] addSubview:_historyLabel];
    
    
    _consolTextView = [[UITextView alloc] initWithFrame:CGRectMake(10.0, 200.0, self.view.frame.size.width - 20.0, 170.0)];
    [[self view] addSubview:_consolTextView];
     */
}

#pragma mark - Actions
- (void)takeHumidityPicAction {
    
    [_humidityPicLabel setText:@""];
    [_hector getHumidityPic];
}

#pragma mark - HectorDelegate
- (void)hectorDidInitialize:(NSError*)error {
    
    if (error) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Connexion", @"")
                                                            message:NSLocalizedString(@"Impossible de se connecter à se périphérique, veuillez réessayer", @"")
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"Fermer", @"")
                                                  otherButtonTitles:nil];
        [alertView show];
        
        [[self navigationController] popViewControllerAnimated:YES];
        
    }else {
        
        // If not on time, alertHour = YES and historic will never start
        if (_hector.alertHour) {
            [_hector setOnTime]; // Clear historic in hector
        }
        
        [_hector setNotification:YES forType:HectorSensorTypeTemperature];
        [_hector setNotification:YES forType:HectorSensorTypeHumidity];
        [_hector readNumberEntriesForType:HectorSensorTypeTemperature];
    }
}

- (void)hectorDidReadTemperature:(float)temperature {
    [_temperatureLabel setText:[NSString stringWithFormat:@"%.2f", temperature]];
}

- (void)hectorDidReadHumidity:(float)humidity {
    [_humidityLabel setText:[NSString stringWithFormat:@"%.0f", humidity]];
}

- (void)hectorDidReadPressure:(float)pressure {
}

- (void)hectorDidReadMinTemperature:(float)minTemperature {
    
}

- (void)hectorDidReadMaxTemperature:(float)maxTemperature {
    
}

- (void)hectorDidReadMinHumidity:(float)minHumidity {
    
}

- (void)hectorDidReadMaxHumidity:(float)maxHumidity {
    
}

- (void)hectorDidReadBattery:(float)battery {
 
    NSString *currentText = [_consolTextView text];
    [_consolTextView setText:[NSString stringWithFormat:@"%@\nBATTERY : %f",currentText,battery]];
}

- (void)hectorDidReadHumidityPic:(float)humidity error:(NSError*)error {
    
    if (error && error.code == 500) {
        [_humidityPicLabel setText:@"Non connecté"];
    }else {
        
        NSString *currentText = [_consolTextView text];
        [_consolTextView setText:[NSString stringWithFormat:@"%@\n%f",currentText,humidity]];
        
        float hum1 = (humidity/100.0);
        float pourcentVal;
        
        if (hum1 >= 0.68 && hum1 < 37.74) {
            
            pourcentVal = 0.00153259*pow(hum1,3)-0.0598387*pow(hum1,2)+0.86172*hum1+14.4412;
            
        }else if (hum1 < 0.68 && hum1 > 0.3){
            
            pourcentVal = 12.363*hum1+6.9873;
            
        }else if (hum1 <= 0.3){
            
            pourcentVal = 0;
            
        }else {
            pourcentVal = MIN(17.178*hum1-604.29,100);
        }
        
        [_humidityPicLabel setText:[NSString stringWithFormat:@"%.0f%% (%.0f) ",pourcentVal,humidity]];
        return;
    }
}

- (void)hectorDidReadName:(NSString*)name {
    [self setTitle:name];
}

- (void)hectorHistoryDidLoad:(NSArray *)values forType:(HectorSensorType)type error:(NSError *)error {
    
    // No data inside Hector
    if ([values count] == 0) {
        
    }
    
    if (type == HectorSensorTypePressure) {
    
        NSLog(@"Pressures : %@", values);
        [_historyLabel setText:@"Historic read ! Check your console"];
        
    }else if (type == HectorSensorTypeTemperature) {
        
        NSLog(@"Temperature : %@", values);
        [_hector getHistoryFromIndex:0 toIndex:(_nbEntriesTemperature/3.0) forType:HectorSensorTypeHumidity];
        
    }else if (type == HectorSensorTypeHumidity) {
        
        NSLog(@"Himiditiy : %@", values);
        [_hector getHistoryFromIndex:0 toIndex:(_nbEntriesTemperature/3.0) forType:HectorSensorTypePressure];
    }
}

- (void)hectorDidReadNumberEntries:(NSInteger)nbValues forType:(HectorSensorType)type {
    
    _nbEntriesTemperature = nbValues;
    
    NSLog(@"Nb Historique : %i", (int)_nbEntriesTemperature);
    
    [_hector getHistoryFromIndex:0 toIndex:(_nbEntriesTemperature/3.0) forType:HectorSensorTypeTemperature];
}

@end
